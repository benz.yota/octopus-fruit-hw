FROM node:16

WORKDIR /app

COPY ./app/package*.json ./

RUN npm install

RUN npm install mongoose

COPY ./app .

EXPOSE 3000

CMD ["npm", "start"]