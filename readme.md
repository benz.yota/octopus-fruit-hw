# My awesome TF module

## Usage
~~~
For thiss CI workflow to work you need to do a couple of things:

** check that your aws user has the right permissions **
1. create a private ECR in aws and change the .gitlab-ci.yml file:
.ex.
- AWS_REGISTRY: <enter your aws regestry> 
  AWS_DEFAULT_REGION: <enter your aws region>
2. Create access key for your aws user and enter them as variables in the gitlab repo:
- go to 'Settings'--> 'CI/CD'-->'Variables'
- create 2 variables "AWS_ACCESS_KEY_ID" and "AWS_DEFAULT_REGION"
3. Create an AMI in aws that already has docker, docker compose and aws cli installed.
- enter the ami type in the terraform.tfvars file
- all the rest of the variables in terraform.tfvars file you can change as you wish
~~~

## Mission

1. You need to provision a new Web application
2. This is a simple “hello world” with a little apples :) page
3. The architecture should include : NodeJS, MongoDB, Containers.
4. The runtime should be on any Linux distro you prefer
5. Every component of the architecture should run in its own container
6. All the architecture should be fully defined by scripts and code (IaC) saved in github 
7. The DB should contain this data: [ { "_id": 1, "name": "apples", "qty": 5, "rating": 3 }, { "_id": 2, "name": "bananas", "qty": 7, "rating": 1, "microsieverts": 0.1 }, { "_id": 3, "name": "oranges", "qty": 6, "rating": 2 }, { "_id": 4, "name": "avocados", "qty": 3, "rating": 5 }, ]
8. The exercise submission should be a git repo with all the scripts and automation code to provision the architecture.
9. The exercise will be checked by running the scripts in the submitted repo.
10. Tests that the exercise should pass:
    Provision from scratch of the entire architecture
    The html page returns the number of apples in the DB 
    Visual demonstration of the architecture (containers, netwrok topology etc..) 

**##Bonus##**
- Full ci/cd pipeline with one of the cloud providers 
- Readme page in the git repo with guid that explains about the app, architecture design etc.. 
- Load balancer 
- Your own ideas will be welcome - create a PULL REQUEST to this branch:)
11. At the end of the exercise you will be asked about the architecture design and the components to demonstrate your ability to learn and understand new areas and technology.