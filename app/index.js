const express = require('express');
const mongoose = require('mongoose');
const app = express();
// Connect to MongoDB with Mongoose
const url = 'mongodb://octopus-mongo-1:27017/apple_db'; // use the name of the MongoDB container as the hostname
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB!'))
    .catch(err => console.error('Error connecting to MongoDB', err));

// Define a Mongoose schema for the data you want to retrieve  { "_id": 1, "name": "apples", "qty": 5, "rating": 3 }
const Schema = mongoose.Schema;

const fruitSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    qty: {
        type: Number,
        required: true
    },
    rating: {
        type: Number,
        required: true
    }
});

// Use the Mongoose model to query the database and retrieve data
const Fruit = mongoose.model('Fruit', fruitSchema, 'apple_tb');


// Define a route to retrieve data from MongoDB and render it on an HTML page
app.get('/', (req, res) => {
    Fruit.findOne({ name: 'apples' })
        .then(result => {
            const qty = result.qty;
            res.status(200).json(qty);
        });
});

// Start the server
app.listen(3000), function () {
    console.log('server is listening on port 3000')
};