terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  # backend "http" {}
}

# Configure the AWS Provider
provider "aws" {
  region = var.project_region
}


terraform {
  backend "s3" {
    bucket = "earbe-bucket-tfstate"
    key    = "global/s3/terraform.tfstate"
    region = "us-east-1"
    # dynamodb_table = "terraform-state-locking"
    encrypt = true
  }
}
